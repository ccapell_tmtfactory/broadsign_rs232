<html>
<body>
<table><thead><tr><td>Operación</td><td>Status</td><td>Return</td><td>Value</td></tr></thead>
    <tbody>
    <?php
    require('vendor/autoload.php');

    use WebSocket\Client;

    $client = new Client("ws://localhost:2326/");

    $deviceActions = [
        ["assert_power" => ["values" => ["1"]]],
        ["power" => ["values" => ["0"]]],
        ["assert_power" => ["values" => ["0"]]],
        ["power" => ["values" => ["1"]]],
        ["assert_power" => ["values" => ["1"]]],
    ];

    foreach ($deviceActions as $actions) {
        foreach ($actions as $action => $values) {
            foreach ($values["values"] as $value) {
                $_command = [
                    "version" => "1",
                    "id" => "1",
                    "action" => "device_operation",
                    "name" => $action,
                    "value" => $value
                ];
                $_payload = ["rc" => $_command];
                $stringified = json_encode($_payload);
                $client->send($stringified);
                printPrettyTable(json_decode($client->receive(), true));
            }
        }
    }

    function printPrettyTable($array) {
        $action = $array["rc"]["device_operation"]["name"];
        $status = $array["rc"]["device_operation"]["status"];
        $return = $array["rc"]["device_operation"]["return"];
        $value = $array["rc"]["device_operation"]["value"];
        print_r("<tr><td>$action</td><td>$status</td><td>$return</td><td>$value</td></tr>");
    }
    ?>

    </tbody>
</table>